Guía práctica de GIT
Imitando lo que viste en los recientes tutoriales, realiza los pasos a continuación. Recuerda que, para comprobar cuánto has aprendido, al final de cada módulo otros participantes medirán tu avance.

1) Instala Git en su ambiente de desarrollo

2) Crea una cuenta gratuita en Bitbucket

3) Crea un repositorio en Bitbucket para utilizarlo en las sucesivas prácticas del curso

4) Crea un repositorio en tu ambiente de desarrollo y vincularlo al repositorio de Bitbucket

5) Como paso final, crea un archivo del estilo README (sólo texto) y súbelo via línea de comandos al repositorio de Bitbucket creado previamente